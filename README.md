本页面收集了墙内镜像。请将此页加为书签。若镜像被封锁，请访问本页面获取最新镜像。

[![x](https://bitbucket.org/greatfire/test/raw/master/renminjianduwang.png "人民监督网 - 免翻墙镜像") **人民监督网**](https://d2b7lajjcl3dv8.cloudfront.net/ "人民监督网 - 免翻墙镜像") | [![x](https://bitbucket.org/greatfire/test/raw/master/lantern.png "以及自由微博和GreatFire.org官方中文论坛 - 免翻墙镜像") **蓝灯/Lantern**](https://d3ie9li8ral8o.cloudfront.net/ "以及自由微博和GreatFire.org官方中文论坛 - 免翻墙镜像")
------------- | -------------
[![x](https://bitbucket.org/greatfire/test/raw/master/nyt.png "纽约时报中文网 国际纵览 - 免翻墙镜像") **纽约时报**](https://d3im9fxhrapv1a.cloudfront.net/ "纽约时报中文网 国际纵览 - 免翻墙镜像") | [![x](https://bitbucket.org/greatfire/test/raw/master/dw.png "德国之声 - 免翻墙镜像") **德国之声**](https://d1iehmrcgpeq4c.cloudfront.net/ "德国之声 - 免翻墙镜像")
[![x](https://bitbucket.org/greatfire/test/raw/master/bbc.png "BBC 中文 - 免翻墙镜像") **BBC 中文**](https://d3jxgdhw7wksio.cloudfront.net/ "BBC 中文 - 免翻墙镜像") | [![x](https://bitbucket.org/greatfire/test/raw/master/freeweibo.png "自由微博 - 匿名和不受屏蔽的新浪微博搜索 - 免翻墙镜像") **自由微博**](https://d2g74y4xy50ozi.cloudfront.net/ "自由微博 - 匿名和不受屏蔽的新浪微博搜索 - 免翻墙镜像")
[![x](https://bitbucket.org/greatfire/test/raw/master/boxun.png "博讯新闻 - 免翻墙镜像") **博讯新闻**](https://boxun34.global.ssl.fastly.net/ "博讯新闻 - 免翻墙镜像") | [![x](https://bitbucket.org/greatfire/test/raw/master/google.png "Google - 免翻墙镜像") **Google**](https://google1.global.ssl.fastly.net/ "Google - 免翻墙镜像")
[![x](https://bitbucket.org/greatfire/test/raw/master/cdt.png "中国数字时代 - 免翻墙镜像") **中国数字时代**](https://cdt2.global.ssl.fastly.net/ "中国数字时代 - 免翻墙镜像") | [![x](https://bitbucket.org/greatfire/test/raw/master/paopao.png "泡泡 - 未经审查的互联网信息 - 免翻墙镜像") **泡泡**](https://pp11.global.ssl.fastly.net/ "泡泡 - 未经审查的互联网信息 - 免翻墙镜像")
[![x](https://bitbucket.org/greatfire/test/raw/master/programthink.png "编程随想的博客 - 免翻墙镜像") **编程随想**](https://d3uj6xm91r4200.cloudfront.net/ "编程随想的博客 - 免翻墙镜像") | 
**自由浏览 - 免翻墙安卓应用**

[![自由浏览](https://bitbucket.org/greatfire/test/raw/master/fb.qr.png "自动翻墙访问被封锁的网站")](https://raw.githubusercontent.com/greatfire/z/master/FreeBrowser-1.1.apk "自动翻墙访问被封锁的网站")

**中国数字时代 - 免翻墙安卓应用**

[![中国数字时代](https://bitbucket.org/greatfire/test/raw/master/cdt.qr.png "中国数字时代")](https://raw.githubusercontent.com/greatfire/z/master/ChinaDigitalTimesAndroid1.6.apk "中国数字时代")

**自由微博 - 免翻墙安卓应用**

[![自由微博](https://bitbucket.org/greatfire/test/raw/master/freeweibo.qr.png "自由微博 - 匿名和不受屏蔽的新浪微博搜索")](https://raw.githubusercontent.com/greatfire/z/master/FreeWeibo1.4.apk "自由微博 - 匿名和不受屏蔽的新浪微博搜索")

**泡泡 - 免翻墙安卓应用**

[![泡泡](https://bitbucket.org/greatfire/test/raw/master/paopao.qr.png "泡泡 | 未经审查的互联网信息")](https://raw.githubusercontent.com/greatfire/z/master/PaoPaoAndroid1.7.apk "泡泡 | 未经审查的互联网信息")


* 如果站点访问不正常，请发邮件到 support@greatfire.org
* [自由微博和GreatFire.org邮件订阅](https://b.us7.list-manage.com/subscribe?u=854fca58782082e0cbdf204a0&id=c78949b93c)
* [点击浏览现有GreatFire开源项目，包括上述镜像网站](https://github.com/greatfire/wiki/wiki)
